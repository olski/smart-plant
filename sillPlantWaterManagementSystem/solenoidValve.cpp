#include "Arduino.h"
#include "solenoidValve.h"

SolenoidValve::SolenoidValve()
{
}

SolenoidValve::SolenoidValve(MCP23017 *mcp, int pin)
{
    this->mcp = mcp;
    this->pin = pin;
    this->isOpened = false;
}

bool SolenoidValve::isOpen()
{
    return this->isOpened;
}

void SolenoidValve::open()
{
    (*mcp).digitalWrite(this->pin, LOW);
    this->isOpened = true;
}

void SolenoidValve::close()
{
    (*mcp).digitalWrite(this->pin, HIGH);
    this->isOpened = false;
}

void SolenoidValve::init(){
    (*mcp).pinMode(this->pin, OUTPUT);
    (*mcp).digitalWrite(this->pin, HIGH);
}
