#include "Arduino.h"
#include "hummiditySensor.h"

HummiditySensor::HummiditySensor()
{}

HummiditySensor::HummiditySensor(Adafruit_ADS1115* ads, int pin, double maxValue, double minValue){
    this->ads = ads;
    this->pin = pin;
    this->maxValue = maxValue;
    this->minValue = minValue;
}

float HummiditySensor::getBoundedValue(){
    int moistValue = (*ads).readADC_SingleEnded(pin);
    if(moistValue < minValue){
        return minValue;
    }else if(moistValue > maxValue){
      return maxValue;
    }
    return moistValue;
}

float HummiditySensor::readValue(){
    int humValue1 = getBoundedValue();
    delay(150);
    int humValue2 = getBoundedValue();
    delay(150);
    int humValue3 = getBoundedValue();

    float result = (humValue1+humValue2)/2.0;

    int min = abs(humValue1-humValue2);
    if(abs(humValue1-humValue3) < min){
        min = abs(humValue1-humValue3);
        result = (humValue1+humValue3)/2.0;
    } 
    if(abs(humValue2-humValue3) < min){
        result = (humValue2+humValue3)/2.0;
    }
    int a = map(result, minValue, maxValue, 100, 0);
    Serial.println((String)pin + " | " + (String)a + "% | " + (String)result); 
    return a;
}
