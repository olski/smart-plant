#ifndef Flower_h
#define Flower_h

#include "Arduino.h"
#include "waterLevelSensor.h"
#include "hummiditySensor.h"
#include "solenoidValve.h"
#include "waterPump.h"
#include "serverWifi.h"
#include <ArduinoJson.h>

class Flower{
    private:    
        int id;      
        int wateringDuration; // in [ms] // How long single watering should last
        unsigned long lastWateringTime;

        unsigned long hummidityCheckFrequency; // how often plant hummidity should be checked
        unsigned long lastHummiditiCheckTime;

        float hummidity; 
        float minHummidity; 

        WaterLevelSensor* waterLevelSensor;
        HummiditySensor* hummiditySensor;
        SolenoidValve* solenoidValve;
        WaterPump* waterPump;
        ServerWifi* serverWifi;
        
        boolean isActive;
        
        void checkHummidity();
        void startWatering();
        void stopWatering();

        boolean shouldCheckHummidity();
        boolean isDry();
        boolean isEnoughWater();
        boolean shouldStopWatering();
        boolean isWateringActive();
        boolean updatePlantData();
        public:
            Flower(int id, String flowerName, unsigned long wateringDuration, unsigned long hummidityCheckFrequency, float minHummidity, WaterLevelSensor* waterLevelSensor, HummiditySensor* hummiditySensor, SolenoidValve* solenoidValve, WaterPump* waterPump, ServerWifi* serverWifi);
            Flower();

            String flowerName;
            void act();
};

#endif
