#include "Arduino.h"
#include "flower.h"

void Flower::checkHummidity(){ 
    hummidity = (*hummiditySensor).readValue();

    (*serverWifi).insertMeasure(id, hummidity);
    lastHummiditiCheckTime = millis();

    Serial.println("Hummidity of: " + (String)flowerName + " is: " + (String)hummidity);
}

void Flower::startWatering(){ 

     
    (*solenoidValve).open();
    delay(200);
    (*waterPump).open();
    lastWateringTime = millis();
    isActive = true;
    (*serverWifi).insertWatering(id);
    Serial.println(flowerName + (String) " is being watered");
}

void Flower::stopWatering(){
    (*waterPump).close();
    delay(200);
    (*solenoidValve).close();

    isActive = false;
    Serial.println(flowerName + (String) " is no longer watered");
}

Flower::Flower(){
}

Flower::Flower(int id, String flowerName, unsigned long wateringDuration, unsigned long hummidityCheckFrequency, float minHummidity, WaterLevelSensor *waterLevelSensor, HummiditySensor *hummiditySensor, SolenoidValve *solenoidValve, WaterPump* waterPump, ServerWifi* serverWifi){
    this->id = id;
    this->minHummidity = minHummidity;
    this->wateringDuration = wateringDuration;

    this->hummidityCheckFrequency = hummidityCheckFrequency;
    this->lastHummiditiCheckTime = 0;
    this->lastWateringTime = 0;

    this->waterLevelSensor = waterLevelSensor;
    this->hummiditySensor = hummiditySensor;
    this->solenoidValve = solenoidValve;
    this->waterPump = waterPump;
    this->serverWifi = serverWifi;
    
    this->isActive = false;
    this->flowerName = flowerName;
}

boolean Flower::shouldCheckHummidity(){
    return (!isWateringActive() && lastHummiditiCheckTime + hummidityCheckFrequency < millis());
}

boolean Flower::isDry(){
    checkHummidity();
    return hummidity < minHummidity;
}

boolean Flower::isWateringActive(){
  return isActive;
}
 
boolean Flower::isEnoughWater(){
    return ((*waterLevelSensor).isEnoughWater());
}

boolean Flower::shouldStopWatering(){
    return (isWateringActive() && lastWateringTime + wateringDuration < millis());
}

boolean Flower::updatePlantData(){
    String payload = (*serverWifi).getFlowerData(id);

    DynamicJsonDocument rootJson(1024);
    deserializeJson(rootJson, payload);
      
    int dataId = rootJson["id"]; 
    if(id != dataId){
      return false;
    }
    String dataName = rootJson["name"];
    int dataMinMoisture = rootJson["minMoisture"];
    int dataWateringDuration = rootJson["wateringDuration"];
    int dataCheckFrequency = rootJson["checkFrequency"];
    
    if(dataName != NULL && dataName != flowerName){
      flowerName = dataName; 
      Serial.println("flowerName:" + (String)flowerName);
    }
    
    if(dataMinMoisture != NULL){
      if(dataMinMoisture != minHummidity){
        minHummidity = dataMinMoisture;
        Serial.println("minHummidity:" + (String)minHummidity);
      }
    }

    if(dataWateringDuration != NULL){
      if(dataWateringDuration*1000 != wateringDuration){
        wateringDuration = dataWateringDuration*1000;
        Serial.println("wateringDuration:" + (String)wateringDuration);
      }
    }

     if(dataCheckFrequency != NULL){
       if(dataCheckFrequency*1000 != hummidityCheckFrequency){
        hummidityCheckFrequency = dataCheckFrequency*1000;
        Serial.println("hummidityCheckFrequency:" + (String)hummidityCheckFrequency);
      }
    }
}

void Flower::act()
{
    updatePlantData();
    boolean isWater = isEnoughWater();
    if (!isWater && isWateringActive())
    {
        stopWatering();
    }
    else if (isWater && shouldCheckHummidity())
    {
        if (isDry())
        {
            startWatering();
        }
    }
    else if (shouldStopWatering())
    {
        stopWatering();
    }
}
