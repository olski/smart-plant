#ifndef SolenoidValve_h
#define SolenoidValve_h

#include "Arduino.h"
#include "MCP23017.h"

class SolenoidValve
{
private:
    int pin;
    bool isOpened;
    MCP23017* mcp;

public:
    SolenoidValve();
    SolenoidValve(MCP23017* mcp, int pin);

    void open();
    void close();
    bool isOpen();
    void init();
};

#endif
