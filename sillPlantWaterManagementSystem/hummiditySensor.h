#ifndef HummiditySensor_h
#define HummiditySensor_h

#include "Arduino.h"
#include <Adafruit_ADS1015.h>

class HummiditySensor
{
private:
    int pin;
    double maxValue;
    double minValue; 
    Adafruit_ADS1115* ads;

    float getBoundedValue();
public:
    HummiditySensor();
    HummiditySensor(Adafruit_ADS1115* ads, int pin, double maxValue, double minValue);

    float readValue();
};

#endif
