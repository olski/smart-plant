#include "Arduino.h"
#include "serverWifi.h"
#include <iostream>

ESP8266WebServer server(80);

ServerWifi::ServerWifi()
{
}

void ServerWifi::handleRoot()
{
    char content[] = "<h3> Witaj na ESP8266. <br>Poniżej znajduje się lista API</h3><ul><li> /podlej?plantId=0</li><li>/wilgotnosc?plantId=0</li></ul>";
    server.send(200, "text/html", content);
}

void ServerWifi::handleNotFound()
{
    String message = "File Not Found\n\n";
    message += "URI: ";
    message += server.uri();
    message += "\nMethod: ";
    message += (server.method() == HTTP_GET) ? "GET" : "POST";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";

    for (uint8_t i = 0; i < server.args(); i++)
    {
        message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    server.send(404, "text/plain", message);
}

void ServerWifi::init()
{
    initConnection();
    initEndpoints();
};

void ServerWifi::initConnection()
{
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    if (MDNS.begin("esp8266"))
    {
        Serial.println("MDNS responder started");
    }
    server.begin();

    Serial.println("");
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
}

void ServerWifi::initEndpoints()
{
    server.onNotFound(std::bind(&ServerWifi::handleNotFound, this));
    server.on("/", std::bind(&ServerWifi::handleRoot, this));

    // server.on("/podlej", [this]() {
    //     if (server.args() < 1)
    //     {
    //         server.send(200, "text/plain", "Brakuje parametr");
    //     }
    //     else
    //     {
    //         for (uint8_t i = 0; i < server.args(); i++)
    //         {
    //             if (server.argName(i) == "id")
    //             {
    //                 Serial.println("Podlewam: " + (String)server.arg(i));
    //                 insertMeasure(server.arg(i).toInt(), 50);
    //             }
    //         }
    //         server.send(200, "text/plain", "murzyn podlewa");
    //     }
    // });
}

void ServerWifi::update()
{
    server.handleClient();
    MDNS.update();
}

void ServerWifi::insertMeasure(int plantId, int humidity)
{
    if (WiFi.status() == WL_CONNECTED)
    {
        HTTPClient http;
        String service = "measure.php";

        String plant = "plantId=" + String(plantId);
        String soilHumidity = "soilHumidity=" + String(humidity);

        String params = plant + "&" + soilHumidity;

        String url = hostName + ":" + port + "/" + service + "?" + params;
        Serial.println(url);

        http.begin(url);
        int httpCode = http.GET();

        if (httpCode > 0)
        {
            String payload = http.getString();
            Serial.println(payload);
        }
        http.end();
    }
}

void ServerWifi::insertWaterLevelState(boolean state)
{
    if (WiFi.status() == WL_CONNECTED)
    {
        HTTPClient http;
        String service = "insertwatertanklevel.php";
        String params = "state=" + String(state);

        String url = hostName + ":" + port + "/" + service + "?" + params;
        Serial.println(url);

        http.begin(url);
        int httpCode = http.GET();

        if (httpCode > 0)
        {
            String payload = http.getString();
            Serial.println(payload);
        }
        http.end();
    }
}

boolean ServerWifi::isSystemTurnedOn()
{
    if (WiFi.status() == WL_CONNECTED)
    {
        HTTPClient http;
        String service = "getpowersupplystate.php";
        String url = hostName + ":" + port + "/" + service;
        http.begin(url);
        int httpCode = http.GET();

        boolean returnState = false;
        if (httpCode > 0)
        {
            String payload = http.getString();
            if(payload == "1"){
              returnState = true;
            }
        }
        http.end();
        return returnState;
    }
}

String ServerWifi::getFlowerData(int flowerId)
{
    String result = "";
    if (WiFi.status() == WL_CONNECTED)
    {
        HTTPClient http;
        String service = "getplantbyid.php";

        String params = "id=" + String(flowerId);
        String url = hostName + ":" + port + "/" + service + "?" + params;
        http.begin(url);
        int httpCode = http.GET();
        boolean returnState = false;
        if (httpCode > 0){
            result = http.getString();
        }
        http.end();
        return result;
    }
}

void ServerWifi::insertWatering(int flowerId)
{
    if (WiFi.status() == WL_CONNECTED)
    {
        HTTPClient http;
        String service = "insertwatering.php";
        String params = "plantId=" + String(flowerId);

        String url = hostName + ":" + port + "/" + service + "?" + params;
        Serial.println(url);

        http.begin(url);
        int httpCode = http.GET();

        if (httpCode > 0)
        {
            String payload = http.getString();
            Serial.println(payload);
        }
        http.end();
    }
}
