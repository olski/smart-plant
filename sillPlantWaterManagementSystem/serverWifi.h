#ifndef ServerWifi_h
#define ServerWifi_h

#include "Arduino.h"
#include "MCP23017.h"
#include <Adafruit_ADS1015.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPClient.h>


class ServerWifi
{
private:
    const char *ssid = "OCANIS_FIBER";
    const char *password = "Ja#17@Ag";
    
    String hostName = "http://87.207.82.229";
    String port = "8080";

    void initConnection();
    void initEndpoints();
    void handleRoot();
    void handleNotFound();

public:
    ServerWifi();
    void init();
    void update();
    void insertMeasure(int plantId, int humidity);
    void insertWaterLevelState(boolean state);
    void insertWatering(int flowerId);
    String getFlowerData(int flowerId);
    boolean isSystemTurnedOn();
    
};

#endif
