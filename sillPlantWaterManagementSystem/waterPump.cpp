#include "Arduino.h"
#include "waterPump.h"

WaterPump::WaterPump()
{
}

WaterPump::WaterPump(MCP23017 *mcp, int pin)
{
    this->mcp = mcp;
    this->pin = pin;
    this->activeSolenoidsCount = 0;
}

void WaterPump::open()
{
    (*mcp).digitalWrite(this->pin, LOW);
    this->activeSolenoidsCount++;
    Serial.println(this->activeSolenoidsCount);
}

void WaterPump::close()
{
    this->activeSolenoidsCount--;
    if(this->activeSolenoidsCount < 1){
        (*mcp).digitalWrite(this->pin, HIGH);
        Serial.println("Turning off pump");
        this->activeSolenoidsCount = 0;
    }
}

void WaterPump::init(){
    (*mcp).pinMode(this->pin, OUTPUT);
    //while(true){
    //(*mcp).digitalWrite(this->pin, LOW);
    delay(1000);
    (*mcp).digitalWrite(this->pin, HIGH);
    //  delay(1000);
    //}
}
