#include <RTClib.h>
#include <Adafruit_ADS1015.h>
#include <stdio.h>
#include <string.h>
#include <Wire.h>

#include "flower.h"
#include "serverWifi.h"
#include "hummiditySensor.h"
#include "solenoidValve.h"
#include "waterPump.h"
#include "waterLevelSensor.h"
#include "MCP23017.h"

MCP23017 mcp;
Adafruit_ADS1115 ads(0x48);
RTC_DS1307 RTC;

bool isWateringActive[4] = {false, false, false};

WaterPump waterPump;

SolenoidValve solenoidValve1;
SolenoidValve solenoidValve2;
SolenoidValve solenoidValve3;

HummiditySensor hummiditySensor1;
HummiditySensor hummiditySensor2;
HummiditySensor hummiditySensor3;

WaterLevelSensor waterLevelSensor;

Flower flower1;
Flower flower2;
Flower flower3;

Flower sillGarden[3];
size_t flowersCount;

ServerWifi serverWifi;

void setup(){
    Serial.begin(115200);
    delay(1000);
    Serial.println("start");

    serverWifi.init();

    Wire.begin(4, 5);

    mcp.begin(7);
    ads.begin();

    RTC.begin();
    RTC.adjust(DateTime(__DATE__, __TIME__));

    waterPump = WaterPump(&mcp, 8);
    waterLevelSensor = WaterLevelSensor(&ads, 3, &serverWifi);

    solenoidValve1 = SolenoidValve(&mcp, 1);
    solenoidValve2 = SolenoidValve(&mcp, 2);
    solenoidValve3 = SolenoidValve(&mcp, 3);

    hummiditySensor1 = HummiditySensor(&ads, 0, 15500, 6000);
    hummiditySensor2 = HummiditySensor(&ads, 1, 10400, 3800);
    hummiditySensor3 = HummiditySensor(&ads, 2, 13200, 7400);

    delay(10000); 

//    waterPump.init();
//
//    solenoidValve1.init();
//    solenoidValve2.init();
//    solenoidValve3.init();
//          
    sillGarden[0] = Flower(0, "Stefan", 3 * 1000, 60 * 1000, 20, &waterLevelSensor, &hummiditySensor1, &solenoidValve1, &waterPump, &serverWifi);
    sillGarden[1] = Flower(1, "Mariusz", 3 * 1000, 60 * 1000, 30, &waterLevelSensor, &hummiditySensor2, &solenoidValve2, &waterPump, &serverWifi);
    sillGarden[2] = Flower(2, "Robert", 3 * 1000, 60 * 1000, 40, &waterLevelSensor, &hummiditySensor3, &solenoidValve3, &waterPump, &serverWifi);
    
    flowersCount = sizeof(sillGarden)/sizeof(sillGarden[0]);
}

boolean isNight(){
    DateTime now = RTC.now();
    return now.hour() >= 22 || now.hour() < 8;
}

void loop(){
    serverWifi.update();
    waterLevelSensor.isEnoughWater();
    
    if (/*!isNight() &&*/ serverWifi.isSystemTurnedOn()){
      for(int i = 0; i< flowersCount; i++){
        sillGarden[i].act();
        delay(1000);
      }
    }
    delay(1000);
}
