#ifndef WaterPump_h
#define WaterPump_h

#include "Arduino.h"
#include "MCP23017.h"

class WaterPump
{
private:
    int pin;
    int activeSolenoidsCount;
    MCP23017* mcp;

public:
    WaterPump();
    WaterPump(MCP23017* mcp, int pin);

    void open();
    void close();
    void init();
};

#endif
