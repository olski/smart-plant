#include "Arduino.h"
#include "waterLevelSensor.h"

WaterLevelSensor::WaterLevelSensor(){
}

WaterLevelSensor::WaterLevelSensor(Adafruit_ADS1115* ads, int pin, ServerWifi* serverWifi){
    this->ads = ads;
    this->pin = pin;
    this->serverWifi = serverWifi;
    this->previousState = false;
}

float WaterLevelSensor::readValue(){
    return (*ads).readADC_SingleEnded(this->pin);
}

boolean WaterLevelSensor::isEnoughWater(){
    boolean state = false;
    if (readValue() > 10){
        state = true;
    }
    if(hasStateChanged(state)){
        insertNewStateToDB(state);
    }
    return state;
}

boolean WaterLevelSensor::hasStateChanged(boolean state){
    if(previousState != state){
        previousState = state;
        return true;
    }
    return false;
}

void WaterLevelSensor::insertNewStateToDB(boolean state){
    (*serverWifi).insertWaterLevelState(state);
}
