#ifndef WaterLevelSensor_h
#define WaterLevelSensor_h

#include "Arduino.h"
#include <Adafruit_ADS1015.h>
#include "serverWifi.h"

class WaterLevelSensor{
    private:
        int pin;
        boolean previousState;

        Adafruit_ADS1115* ads;
        ServerWifi* serverWifi;

        void insertNewStateToDB(boolean state);
        boolean hasStateChanged(boolean state);

    public:
        WaterLevelSensor();
        WaterLevelSensor(Adafruit_ADS1115* ads, int pin, ServerWifi* serverWifi);

        float readValue();
        boolean isEnoughWater();
};

#endif
