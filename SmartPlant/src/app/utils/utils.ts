import { MatSnackBar } from '@angular/material/snack-bar';

export class Utils {
    constructor() {
    }

    static parseDateToISO(date: Date) {
        return date.toISOString().slice(0, 19).replace('T', ' ');
    }

    static getLastWeekDate() {
        var lastWeekTime = new Date();
        lastWeekTime.setDate(lastWeekTime.getDate() - 7);

        return this.parseDateToISO(lastWeekTime);
    }

    static getLastDayDate() {
        let lastDayTime = new Date();
        lastDayTime.setHours(lastDayTime.getHours() - 24);

        return this.parseDateToISO(lastDayTime);
    }

    static openSnackBar(snackBar: MatSnackBar, message: string) {
        snackBar.open(message, undefined, {
            duration: 2000,
            panelClass: ['mat-simple-snackbar']
        });
    }
}


