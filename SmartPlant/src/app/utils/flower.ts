export class Flower {
    id: number;
    name: string;
    minMoisture: number;
    imgName: string;
    actualSoilMoisture: number;
    wateringDuration: number;
    checkFrequency: number;
    lastWateringTime: string;
    lastWateringDate: string;

    constructor(id: number, name: string, minMoisture: number, imgName: string, wateringDuration: number, checkFrequency: number) {
        this.id = id;
        this.name = name;
        this.minMoisture = minMoisture;
        this.imgName = imgName;
        this.actualSoilMoisture = 0;
        this.wateringDuration = wateringDuration;
        this.checkFrequency = checkFrequency;
        this.lastWateringTime = "";
        this.lastWateringDate = "";
    }
}