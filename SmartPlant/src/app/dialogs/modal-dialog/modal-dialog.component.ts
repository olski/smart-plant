import { Component, OnInit, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Flower } from '../../utils';

@Component({
  selector: 'app-dialog-body',
  templateUrl: './modal-dialog.component.html',
  styleUrls: ['./modal-dialog.component.css']
})
export class DialogBodyComponent implements OnInit {
  option : string = "today";

  constructor(public dialogRef: MatDialogRef<DialogBodyComponent>, @Inject(MAT_DIALOG_DATA) public flower: Flower){}

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }
}
