import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HomeComponent } from './home/';
import { FooterComponent } from './layout/footer/';
import { HeaderComponent } from './layout/header/';
import { FlowerCardComponent } from './flower-card/';
import { FlowerPropertiesPaneComponent } from './flower-card/flower-properties-pane/';
import { SystemStatusMonitComponent } from './system-status-monit/';
import { LineChartComponent } from './charts/line-chart/';
import { DialogBodyComponent } from './dialogs/modal-dialog/';

import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { ChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    FlowerCardComponent,
    FlowerPropertiesPaneComponent,
    SystemStatusMonitComponent,
    LineChartComponent,
    DialogBodyComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    ChartsModule,
    MatDialogModule,
    MatButtonModule,
    MatDividerModule,
    MatRadioModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
