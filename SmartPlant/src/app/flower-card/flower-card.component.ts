import { Component, OnInit, Input } from '@angular/core';
import { Flower } from '../utils';

import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';

import { DialogBodyComponent } from '../dialogs/modal-dialog';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Component({
  selector: 'flower-card',
  templateUrl: './flower-card.component.html',
  styleUrls: ['./flower-card.component.css']
})
export class FlowerCardComponent implements OnInit {
  @Input() flower: Flower;
  isSmall: boolean = false;
  ngOnInit() {
  }

  constructor(private matDialog: MatDialog, private breakpointObserver: BreakpointObserver) {
    this.flower = new Flower(0,"",0,"",0,0);

    breakpointObserver.observe([
      '(max-width: 768px)'
        ]).subscribe(result => {
          if (result.matches) {
            this.isSmall = true;
          }
        });
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    if(this.isSmall){
      dialogConfig.minWidth = "95%";
    }else{
      dialogConfig.width = "90%";
    }
    dialogConfig.minHeight = "50%";
    dialogConfig.data =  this.flower;
    
    this.matDialog.open(DialogBodyComponent, dialogConfig);
  }
}