import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { DatePipe } from '@angular/common';

import { DatabaseService } from '../../service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Utils, Flower} from '../../utils';

@Component({
    selector: 'app-flower-properties-pane',
    templateUrl: './flower-properties-pane.component.html',
    styleUrls: ['./flower-properties-pane.component.css']
})
export class FlowerPropertiesPaneComponent implements OnInit {
    @Input() flower: Flower;

    @Input()
    soilMoistureObservable$: Observable<any>;

    @Input()
    lastWateringsObservable$: Observable<any>;

    newName: string;
    newMoisture: string;
    newFrequency: number;
    newWateringDuration: number;
    isInitialized: boolean;

    constructor(private measurementsService: DatabaseService, private _snackBar: MatSnackBar) {
        this.flower = new Flower(0, "", 0, "", 0, 0);
        this.newName = "";
        this.newMoisture = "";
        this.newFrequency = 0;
        this.newWateringDuration = 0;
        this.isInitialized = false;

        this.soilMoistureObservable$ = measurementsService.getLastMeasurements();
        this.lastWateringsObservable$ = measurementsService.getLastWaterings();
    }

    ngOnInit(): void {
        this.refresh();
    }

    refresh(): void {
        this.soilMoistureObservable$.subscribe((data: any[]) => {
            data.forEach(item => {
                if (item.id == this.flower.id)
                    this.flower.actualSoilMoisture = item.soilMoisture;
            });
        })

        this.lastWateringsObservable$.subscribe((data: any[]) => {
            data.forEach(item => {
                if (item.plantId == this.flower.id) {
                    const datepipe: DatePipe = new DatePipe('en-US')
                    let formattedDate = datepipe.transform(item.time, 'HH:mm:ss');
                    if (formattedDate)
                        this.flower.lastWateringTime = formattedDate.toLocaleString();

                    formattedDate = datepipe.transform(item.time, 'dd MMM YYYY');
                    if (formattedDate)
                        this.flower.lastWateringDate = formattedDate.toLocaleString();
                }
            });
            
        })
        if(this.isInitialized){
            Utils.openSnackBar(this._snackBar,"Refreshed successfuly");
        }
        this.isInitialized = true;
        this.newFrequency = this.flower.checkFrequency;
        this.newWateringDuration = this.flower.wateringDuration;


    }

    setMoisture() {
        if (isNaN(parseInt(this.newMoisture)) || parseInt(this.newMoisture) < 0 || parseInt(this.newMoisture) > 100) {
            Utils.openSnackBar(this._snackBar,"You must enter a number within range of 0-100");
        } else {
            this.measurementsService.alterPlantData(this.flower.id, "minMoisture", this.newMoisture).then(
                data => {
                    if (data.code == 200) {
                        this.flower.minMoisture = parseInt(this.newMoisture);
                        Utils.openSnackBar(this._snackBar, "Successfully updated.");
                    } else {
                        Utils.openSnackBar(this._snackBar, "Something went wrong, please try again later");
                    }
                }
            );
        }
    }

    setName() {
        //http://87.207.82.229:8080/alterplantdata.php?plantId=0&name=Asd"
        if (this.newName == "") {
            Utils.openSnackBar(this._snackBar, "You must enter a valid string");
        } else {
            this.newName = this.capitalizeFirstLetter(this.newName);
            this.measurementsService.alterPlantData(this.flower.id, "name", this.newName).then(
                data => {
                    if (data.code == 200) {
                        this.flower.name = this.newName;
                        this.newName = "";
                        Utils.openSnackBar(this._snackBar, "Successfully updated.");
                    } else {
                        Utils.openSnackBar(this._snackBar, "Something went wrong, please try again later");
                    }
                }
            );
        }
    }

    setFrequency() {
        if (isNaN(this.newWateringDuration) || this.newFrequency < 1 || this.newFrequency > 3600) {
            Utils.openSnackBar(this._snackBar, "You must enter a valid number in range 1-3600 (seconds)");
        } else {
            this.measurementsService.alterPlantData(this.flower.id, "checkFrequency", this.newFrequency.toString()).then(
                data => {
                    if (data.code == 200) {
                        this.flower.checkFrequency = this.newFrequency;
                        Utils.openSnackBar(this._snackBar, "Successfully updated.");
                    } else {
                        Utils.openSnackBar(this._snackBar, "Something went wrong, please try again later");
                    }
                }
            );
        }
    }

    setWateringDuration() {
        if (isNaN(this.newWateringDuration) ||  this.newWateringDuration < 1 || this.newWateringDuration > 15) {
            Utils.openSnackBar(this._snackBar, "You must enter a valid number in range 1-15 (seconds)");
        } else {
            this.measurementsService.alterPlantData(this.flower.id, "wateringDuration", this.newWateringDuration.toString()).then(
                data => {
                    if (data.code == 200) {
                        this.flower.wateringDuration = this.newWateringDuration;
                        this.newWateringDuration = 0;
                        Utils.openSnackBar(this._snackBar, "Successfully updated.");
                    } else {
                        Utils.openSnackBar(this._snackBar, "Something went wrong, please try again later");
                    }
                }
            );
        }
    }

    capitalizeFirstLetter(string: string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

}
