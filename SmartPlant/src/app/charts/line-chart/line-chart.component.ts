import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';

import { DatabaseService } from '../../service';
import { Flower } from '../../utils';

import { MatRadioModule } from '@angular/material/radio';

@Component({
    selector: 'line-chart',
    templateUrl: './line-chart.component.html',
    styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent {

    @Input() flower: Flower;


    private _option: string = '';
    @Input('option')
    set name(option: string) {
        this._option = option;
        this.updateChartData();
    }
    @Input()
    result$: Observable<any>;

    measurements: any[] = [];
    timeStamps: any[] = [];

    lineChartData: ChartDataSets[] = [];
    lineChartLabels: Label[] = [];

    constructor(private measurementsService: DatabaseService) {
        this.flower = new Flower(0, "", 0, "", 0, 0);
        this.result$ = measurementsService.getLastDayMeasurements(this.flower.id);
    }

    ngOnInit() {
    }

    updateChartData() {
        switch (this._option) {
            case "today": {
                this.result$ = this.measurementsService.getLastDayMeasurements(this.flower.id);
                break;
            }
            case "last_week": {
                this.result$ = this.measurementsService.getLastWeekMeasurements(this.flower.id);
                break;
            }
            default:
                break;
        }
        this.result$.subscribe((data: any[]) => this.parseChartData(data));

    }

    parseChartData(data: any[]) {
        var measurementsArr = data.map(function (item) {
            return item.soilMoisture;
        });
        var timeStampsArr = data.map(function (item) {
            return item.time;
        });
            
        let bulletsCount = 100;
        let delta = Math.round(measurementsArr.length/bulletsCount);

        this.measurements = this.getNthArrayElement(measurementsArr, delta);
        this.timeStamps = this.getNthArrayElement(timeStampsArr, delta);

        this.lineChartData = [
            { data: this.measurements, label: this.flower.name },
        ];
        this.lineChartLabels = this.timeStamps;
    }

    getNthArrayElement(sourceArray: any[], delta: number) {
        if(sourceArray.length < 500){
            return sourceArray;
        }
        let newArray: any[] = [];
        for (var i = 0; i < sourceArray.length; i = i + delta) {
            newArray.push(sourceArray[i]);
        }
        return newArray;
    };

    lineChartOptions: ChartOptions = {
        responsive: true
    };

    lineChartColors: Color[] = [
        {
            backgroundColor: 'rgba(0,255,0,0.4)',
            borderColor: 'green',
        }
    ];

    lineChartLegend = true;
    lineChartType: ChartType = "line";
    lineChartPlugins = [];
}