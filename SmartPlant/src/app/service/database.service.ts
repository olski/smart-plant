import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Utils } from '../utils';

@Injectable({
    providedIn: 'root'
})
export class DatabaseService {
    private readonly URL = 'http://87.207.81.46:8080/';

    constructor(private http: HttpClient) { }


    public getLastMeasurements(): Observable<any> {
        let serviceName: string = "getlastmeasures.php";

        return this.http.get(this.URL + serviceName);
    }
    
    public alterPlantData(plantId: number, propName: string, newValue: string): Promise<any> {
        let serviceName: string = "alterplantdata.php";
        let url = this.URL + serviceName + "?plantid=" + plantId + "&propname=" + propName + "&propvalue=" + newValue;
        return this.http.get(encodeURI(url)).toPromise()
            .then((resp: any) => {
                return resp;
            });
    }

    getLastDayMeasurements(id: number): Observable<any> {
        let serviceName: string = "getmeasures.php";

        let lastDayString = Utils.getLastDayDate();

        let params: string = "?plantId=" + id + "&datefrom=" + lastDayString + "&dateto=" + '';

        let url = encodeURI(this.URL + serviceName + params)
        return this.http.get(url);
    }

    getLastWaterings(): Observable<any> {
        let serviceName: string = "getlastwaterings.php";

        let url = encodeURI(this.URL + serviceName)
        return this.http.get(url);
    }

    getLastWeekMeasurements(id: number): Observable<any> {
        let serviceName: string = "getmeasures.php";

        let lastWeekTimeString = Utils.getLastWeekDate();
        let currentTimeString = Utils.parseDateToISO(new Date());

        let params: string = "?plantId=" + id + "&datefrom=" + lastWeekTimeString + "&dateto=" + currentTimeString;

        let url = encodeURI(this.URL + serviceName + params)
        console.log(url);

        return this.http.get(url);
    }

    getPlants(): Observable<any> {
        let serviceName: string = "getplants.php";

        return this.http.get(this.URL + serviceName);
    }

    getWaterTankStatus(): Observable<any> {
        let serviceName: string = "getwatertanklevel.php";

        return this.http.get(this.URL + serviceName);
    }

    getPowerSupplyStatus(): Observable<any> {
        let serviceName: string = "getpowersupplystate.php";

        return this.http.get(this.URL + serviceName);
    }
    
    public turnOnOffSystem(state: string): Observable<any> {
        let serviceName: string = "insertpowersupply.php";
        let url = this.URL + serviceName + "?state=" + state;
        return this.http.get(encodeURI(url));
        // return this.http.get(encodeURI(url)).toPromise()
        //     .then((resp: any) => {
        //         return resp;
        //     });
    }
}
