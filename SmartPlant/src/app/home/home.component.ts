import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DatabaseService } from '../service';
import { Flower } from '../utils';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
    flowersArray: Array<Flower> = [];

    @Input()
    plantsObservable$: Observable<any>;

    constructor(private measurementsService: DatabaseService) {
        this.plantsObservable$ = measurementsService.getPlants();
    }

    ngOnInit() {
        this.plantsObservable$.subscribe((data: any[]) => {
            data.forEach(item => {
                this.flowersArray.push(new Flower(item.id, item.name, item.minMoisture, item.imgName, item.wateringDuration, item.checkFrequency));
            });
        });
    }
}


