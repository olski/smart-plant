import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';

import { DatabaseService } from '../service';

import { MatSnackBar } from '@angular/material/snack-bar';
import { Utils } from '../utils';

@Component({
    selector: 'system-status-monit',
    templateUrl: './system-status-monit.component.html',
    styleUrls: ['./system-status-monit.component.css']
})
export class SystemStatusMonitComponent implements OnInit {

    waterLevelStatus: string;
    powerSupplyStatus: string;

    @Input()
    waterTankObservable$: Observable<any>;
    powerSupplyObservable$: Observable<any>;


    intervalId: any;
    constructor(private databaseService: DatabaseService, private _snackBar: MatSnackBar) {
        this.waterLevelStatus = "0";
        this.powerSupplyStatus = "0";
        this.waterTankObservable$ = databaseService.getWaterTankStatus();
        this.powerSupplyObservable$ = databaseService.getPowerSupplyStatus();
    }

    ngOnInit(): void {
        this.getWaterTankStatus();
        this.getPowerSupplyStatus();
        this.intervalId = setInterval(() => {
            this.getWaterTankStatus();
            this.getPowerSupplyStatus();
        }, 5000);
    }

    ngOnDestroy(): void {
        if (this.intervalId) {
            clearInterval(this.intervalId);
        }
    }

    getWaterTankStatus() {
        this.waterTankObservable$.subscribe((data: any[]) => {
            if (data.length > 0) {
                this.waterLevelStatus = data[0].read_signal;
            }
        })
    }

    getPowerSupplyStatus() {
        this.powerSupplyObservable$.subscribe((data: any) => {
            if(data == null)
                data = "0";
            this.powerSupplyStatus = data;
        })
    }

    turnOnOffSystem(){
        let newState = "0";
        if(this.powerSupplyStatus == "0")
            newState = "1";
        this.databaseService.turnOnOffSystem(newState).subscribe((data: any) => {
            this.powerSupplyStatus = data;
            if(data == "0")
                Utils.openSnackBar(this._snackBar, "System turned off.");
            else
                Utils.openSnackBar(this._snackBar, "System turned on.");
        })
    }

}
